package pl.pinkos.rm.model.enums;

public enum AcceptanceStatus {
	ACCEPTED, NEED_ACCEPTATION, REJECTED;

}
