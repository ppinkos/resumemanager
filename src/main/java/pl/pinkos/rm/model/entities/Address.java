package pl.pinkos.rm.model.entities;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import pl.pinkos.rm.model.AbstractEntity;

@Entity
@Table(name="addresses")
public class Address extends AbstractEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private long id;
	
	@NotNull(message="{message.validation.address.country.notNull}")
	@Size(min=3, max=100, message="{message.validation.address.country.size}")
	@Column(name="country", length=100)
	private String country;
	
	@NotNull(message="{message.validation.address.city.notNull}")
	@Size(min=3, max=100, message="{message.validation.address.city.size}")
	@Column(name="city", length=100)
	private String city;
	
	@NotNull(message="{message.validation.address.postCode.notNull}")
	@Size(min=2, max=30, message="{message.validation.address.postCode.size}")
	@Column(name="post_code", length=30)
	private String postCode;
	
	@NotNull(message="{message.validation.address.street.notNull}")
	@Size(min=3, max=100, message="{message.validation.address.street.size}")
	@Column(name="street", length=100)
	private String street;
	
	@NotNull(message="{message.validation.address.estateNumber.notNull}")
	@Size(min=1, max=10, message="{message.validation.address.estateNumber.size}")
	@Column(name="estate_number", length=10)
	private String estateNumber;
	
	@OneToOne(mappedBy="address", cascade=CascadeType.ALL, fetch=FetchType.EAGER)
	private Person person;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getPostCode() {
		return postCode;
	}

	public void setPostCode(String postCode) {
		this.postCode = postCode;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String getEstateNumber() {
		return estateNumber;
	}

	public void setEstateNumber(String estateNumber) {
		this.estateNumber = estateNumber;
	}

	public Person getPerson() {
		return person;
	}

	public void setPerson(Person person) {
		this.person = person;
	}

	@Override
	public String toString() {
		return "Address [id=" + id + ", version=" + version + "]";
	}


	
	
}
