package pl.pinkos.rm.model.entities;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.Email;

import pl.pinkos.rm.model.AbstractEntity;

@Entity
@Table(name="users")
@NamedQueries({
	@NamedQuery(name="User.findUserByName", query="select u from User u WHERE u.userName = ?1"),
	@NamedQuery(name="User.findAll", query="select u from User u"),
	@NamedQuery(name="User.findNotActivated", query="select u from User u where u.userActivated <> true")
})
public class User extends AbstractEntity {
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private long id;
	
	@NotNull(message="{message.validation.user.userName.notNull}")
	@Size(min=3, max=100, message="{message.validation.user.userName.size}")
	@Column(name="user_name", unique=true, nullable=false, updatable=false, length=100)
	private String userName;
	
	@NotNull(message="{message.validation.user.password.notNull}")
	@Size(min=8, max=100, message="{message.validation.user.password.size}")
	@Column(name="password", nullable=false, length=100)
	private String password;
	
	@NotNull
	@Column(name="email_address", nullable=false)
	@Email(message="{message.validation.user.email.email}")
	private String email;
	
	@NotNull
	@Column(name="user_activated", nullable=false)
	private boolean userActivated;
	
	@NotNull
	@Column(name="user_lock", nullable=false)
	private boolean userLock;
	
	@ManyToOne
	@JoinColumn(name="role_id", referencedColumnName="id")
	private Role userRole;
	
	@OneToOne(cascade=CascadeType.ALL, fetch=FetchType.EAGER)
	@JoinColumn(name="person_id")
	private Person person;

	
	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public boolean isUserActivated() {
		return userActivated;
	}

	public void setUserActivated(boolean userActivated) {
		this.userActivated = userActivated;
	}

	public boolean isUserLock() {
		return userLock;
	}

	public void setUserLock(boolean userLock) {
		this.userLock = userLock;
	}

	public Role getUserRole() {
		return userRole;
	}

	public void setUserRole(Role userRole) {
		this.userRole = userRole;
	}

	public long getId() {
		return id;
	}

	public Person getPerson() {
		return person;
	}

	public void setPerson(Person person) {
		this.person = person;
	}
	

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Override
	public String toString() {
		return "User [id=" + id + ", userName=" + userName + ", version=" + version + "]";
	}
	

	
}
