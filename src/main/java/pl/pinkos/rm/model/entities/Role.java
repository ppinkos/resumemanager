package pl.pinkos.rm.model.entities;



import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import pl.pinkos.rm.model.AbstractEntity;

@Entity
@Table(name="roles")
@NamedQueries({
	@NamedQuery(name="Role.findAll", query="select r from Role r"),
	@NamedQuery(name="Role.findByName", query="select r from Role r WHERE r.roleName = ?1")
})
public class Role extends AbstractEntity {

	@Id
	private long id;
	
	@Column(name="role_name", nullable=false, unique=true)
	private String roleName;
	
	@OneToMany(mappedBy="userRole", fetch=FetchType.EAGER)
	private List<User> users;

	public String getRoleName() {
		return roleName;
	}

	public void setRoleName(String roleName) {
		this.roleName = roleName;
	}

	public List<User> getUsers() {
		return users;
	}

	public void setUsers(List<User> users) {
		this.users = users;
	}

	public long getId() {
		return id;
	}

	@Override
	public String toString() {
		return "Role [id=" + id + ", version=" + version + "]";
	}




	


}
