package pl.pinkos.rm.model.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import pl.pinkos.rm.model.AbstractEntity;

@Entity
@Table(name="experience")
@NamedQueries({
	@NamedQuery(name="Experience.findAllByPerson", query="select e from Experience e where e.person = ?1 order by e.fromDate desc"),
	@NamedQuery(name="Experience.getExperienceForAcceptation", query="select e from Experience e where e.person = ?1 and e.accepted=false"),
})
public class Experience extends AbstractEntity {
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private long id;
	
	@NotNull(message="{message.validation.experience.title.notNull}")
	@Size(min=3, max=100, message="{message.validation.experience.title.size}")
	@Column(name="title", length=100)
	private String title;
	
	@NotNull(message="{message.validation.experience.company.notNull}")
	@Size(min=2, max=200, message="{message.validation.experience.company.size}")
	@Column(name="company", length=200)
	private String company;
	
	@NotNull(message="{message.validation.experience.industry.notNull}")
	@Size(min=2, max=100, message="{message.validation.experience.industry.size}")
	@Column(name="industry", length=100)
	private String industry;
	
	@NotNull(message="{message.validation.experience.fromDate.notNull}")
	@Column(name="from_date")
	private Date fromDate;
	
	@Column(name="to_date")
	private Date toDate;
	
	@Column(name="description", length=4000)
	private String description;
	
	@ManyToOne(fetch=FetchType.EAGER, optional=false)
	@JoinColumn(name="person_id")
	private Person person;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getCompany() {
		return company;
	}

	public void setCompany(String company) {
		this.company = company;
	}

	public String getIndustry() {
		return industry;
	}

	public void setIndustry(String industry) {
		this.industry = industry;
	}

	public Date getFromDate() {
		return fromDate;
	}

	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}

	public Date getToDate() {
		return toDate;
	}

	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Person getPerson() {
		return person;
	}

	public void setPerson(Person person) {
		this.person = person;
	}

	@Override
	public String toString() {
		return "Experience [id=" + id + ", version=" + version + "]";
	}


	
	
	
}
