package pl.pinkos.rm.model.entities;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import pl.pinkos.rm.model.AbstractEntity;
import pl.pinkos.rm.model.enums.AcceptanceStatus;

@Entity
@Table(name="persons")
@NamedQueries({
	@NamedQuery(name="Person.findByUser",query="select p from Person p WHERE p.id = ?1"),
	@NamedQuery(name="Person.findPersonsNeedAcceptaion",query="select p from Person p WHERE p.resumeAcceptanceStatus = pl.pinkos.rm.model.enums.AcceptanceStatus.NEED_ACCEPTATION order by p.updatedAt asc"),
	@NamedQuery(name="Person.searchPersonBySurname",query="select p from Person p WHERE upper(p.surName) like ?1 order by p.surName asc"),
})
public class Person extends AbstractEntity {
	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE)
	private long id;
	
	@NotNull(message="{message.validation.person.name.notNull}")
	@Size(min=2, max=100, message="{message.validation.person.name.size}")
	@Column(name="name", length=100)
	private String name;
	
	@NotNull(message="{message.validation.person.surname.notNull}")
	@Size(min=2, max=150, message="{message.validation.person.surname.size}")
	@Column(name="surname", length=150)
	private String surName;
	
	@OneToOne(cascade=CascadeType.ALL, fetch=FetchType.EAGER)
	@JoinColumn(name="address_id")
	private Address address;
	
	@OneToOne(mappedBy="person", optional=false)
	private User user;
	
	@Enumerated(EnumType.STRING)
	@Column(name="resume_accept_status")
	private AcceptanceStatus resumeAcceptanceStatus;
	
	@Column(name="rejection_note")
	private String rejectionReason;
	

	public long getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurName() {
		return surName;
	}

	public void setSurName(String surName) {
		this.surName = surName;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public AcceptanceStatus getResumeAcceptanceStatus() {
		return resumeAcceptanceStatus;
	}

	public void setResumeAcceptanceStatus(AcceptanceStatus resumeAcceptanceStatus) {
		this.resumeAcceptanceStatus = resumeAcceptanceStatus;
	}

	public String getRejectionReason() {
		return rejectionReason;
	}

	public void setRejectionReason(String rejectionReason) {
		this.rejectionReason = rejectionReason;
	}

	@Override
	public String toString() {
		return "Person [id=" + id + ", version=" + version + "]";
	}




}
