package pl.pinkos.rm.model.entities;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import pl.pinkos.rm.model.AbstractEntity;

@Entity
@Table(name="education")
@NamedQueries({
	@NamedQuery(name="Education.findAllByPerson", query="select e from Education e where e.person = ?1 order by e.fromDate desc"),
	@NamedQuery(name="Education.getEducationForAcceptation", query="select e from Education e where e.person = ?1 and e.accepted=false"),
})
public class Education extends AbstractEntity {
	
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private long id;
	
	@NotNull(message="{message.validation.education.school.notNull}")
	@Size(min=5, max=250, message="{message.validation.education.school.size}")
	@Column(name="school", length=250)
	private String school;
	
	@NotNull(message="{message.validation.education.fieldOfStudy.notNull}")
	@Size(min=2, max=250, message="{message.validation.education.fieldOfStudy.size}")
	@Column(name="field_of_study", length=250)
	private String fieldOfStudy;
	
	@Size(min=0, max=50, message="{message.validation.education.degree.size}")
	@Column(name="degree", length=50)
	private String degree;
	
	@NotNull(message="{message.validation.education.fromDate.notNull}")
	@Column(name="from_date")
	private Date fromDate;
	
	@Column(name="to_date")
	private Date toDate;
	
	@ManyToOne(fetch=FetchType.EAGER, optional=false)
	@JoinColumn(name="person_id")
	private Person person;
	
	@Column(name="language")
	private String language;
	

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getSchool() {
		return school;
	}

	public void setSchool(String school) {
		this.school = school;
	}

	public String getFieldOfStudy() {
		return fieldOfStudy;
	}

	public void setFieldOfStudy(String fieldOfStudy) {
		this.fieldOfStudy = fieldOfStudy;
	}

	public String getDegree() {
		return degree;
	}

	public void setDegree(String degree) {
		this.degree = degree;
	}

	public Date getFromDate() {
		return fromDate;
	}

	public void setFromDate(Date fromDate) {
		this.fromDate = fromDate;
	}

	public Date getToDate() {
		return toDate;
	}

	public void setToDate(Date toDate) {
		this.toDate = toDate;
	}

	public Person getPerson() {
		return person;
	}

	public void setPerson(Person person) {
		this.person = person;
	}


	public String getLanguage() {
		return language;
	}

	public void setLanguage(String language) {
		this.language = language;
	}

	@Override
	public String toString() {
		return "Education [id=" + id + ", version=" + version + "]";
	}




	

}
