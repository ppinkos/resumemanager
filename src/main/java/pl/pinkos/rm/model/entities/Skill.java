package pl.pinkos.rm.model.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import pl.pinkos.rm.model.AbstractEntity;

@Entity
@Table(name="skills")
@NamedQueries({
	@NamedQuery(name="Skill.findAllByPerson", query="select s from Skill s where s.person = ?1 order by s.skillLevel desc"),
	@NamedQuery(name="Skill.getSkillForAcceptation", query="select s from Skill s where s.person = ?1 and s.accepted=false"),
	@NamedQuery(name="Skill.getDistinctSkillsNames", query="select distinct(s.skillName) from Skill s order by s.skillName"),
	@NamedQuery(name="Skill.findPersonBySkills", query="select distinct(s.person) from Skill s where s.skillName IN ?1"),
})
public class Skill extends AbstractEntity {

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE)
	private long id;
	
	@NotNull(message="{message.validation.skill.skillName.notNull}")
	@Size(min=2, max=50, message="{message.validation.skill.skillName.size}")
	@Column(name="skill_name", length=50)
	private String skillName;
	
	@NotNull(message="{message.validation.skill.skillLevel.notNull}")
	@Column(name="skill_level")
	@Min(value=1, message="{message.validation.skill.skillLevel.min}")
	@Max(value=5, message="{message.validation.skill.skillLevel.max}")
	private int skillLevel;
	
	@ManyToOne(fetch=FetchType.EAGER, optional=false)
	@JoinColumn(name="person_id")
	private Person person;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getSkillName() {
		return skillName;
	}

	public void setSkillName(String skillName) {
		this.skillName = skillName;
	}

	public int getSkillLevel() {
		return skillLevel;
	}

	public void setSkillLevel(int skillLevel) {
		this.skillLevel = skillLevel;
	}

	public Person getPerson() {
		return person;
	}

	public void setPerson(Person person) {
		this.person = person;
	}

	@Override
	public String toString() {
		return "Skill [id=" + id + ", version=" + version + "]";
	}



	
	
	
	
}
