package pl.pinkos.rm.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.MappedSuperclass;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;
import javax.persistence.Version;

import pl.pinkos.rm.web.utils.ContextUtils;

@MappedSuperclass
@Inheritance(strategy=InheritanceType.TABLE_PER_CLASS)
public abstract class AbstractEntity {
	
	@Version
	protected int version;
	
	@Column(name="created_at", updatable=false)
	private Date createdAt;
	
	@Column(name="created_by", updatable=false)
	private String createdBy;
	
	@Column(name="updated_at", updatable=true)
	private Date updatedAt;
	
	@Column(name="updated_by", updatable=true)
	private String updatedBy;
	
	@Column(name="accepted", columnDefinition="boolean default false")
	private boolean accepted;

	public int getVersion() {
		return version;
	}

	public boolean isAccepted() {
		return accepted;
	}

	public void setAccepted(boolean accepted) {
		this.accepted = accepted;
	}
	
	@PrePersist
	private void prePersist(){
		createdAt = new Date();
		createdBy = ContextUtils.getLoggedUserName();
		updatedAt = new Date();
		updatedBy = ContextUtils.getLoggedUserName();
	}
	
	@PreUpdate
	private void preUpdate(){
		updatedAt = new Date();
		updatedBy = ContextUtils.getLoggedUserName();
	}
	
	
	
	

}
