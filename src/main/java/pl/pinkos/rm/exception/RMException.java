package pl.pinkos.rm.exception;

import javax.mail.MessagingException;
import javax.persistence.OptimisticLockException;

import pl.pinkos.rm.model.AbstractEntity;


public class RMException extends AbstractRMException {
	
	public static final String KEY_OPTIMISTIC_LOCK = "exception.optimisticlock";
	public static final String EMAIL_SEND_ERROR = "exception.emailError";
	public static final String PDF_GENERATION_ERROR = "exception.pdfGeneration";
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -4265472484995435701L;
	
	private RMException(String message) {
		super(message);
	}
	
	private RMException(String message, Throwable cause){
		super(message, cause);
	}
	
	private AbstractEntity entity;
	
	public static RMException createExceptionWithOptimisticLockKey(AbstractEntity entity, OptimisticLockException cause){
		RMException ex = new RMException(KEY_OPTIMISTIC_LOCK, cause);
		ex.setEntity(entity);
		return ex;		
	}
	
	public static RMException createExceptionWithEmailSendError(AbstractEntity entity, MessagingException cause){
		RMException ex = new RMException(EMAIL_SEND_ERROR, cause);
		ex.setEntity(entity);
		return ex;		
	}
	
	public static RMException createExceptionWithPDFGeneration(Exception cause){
		RMException ex = new RMException(PDF_GENERATION_ERROR, cause);
		return ex;		
	}

	public AbstractEntity getEntity() {
		return entity;
	}

	public void setEntity(AbstractEntity entity) {
		this.entity = entity;
	}

}
