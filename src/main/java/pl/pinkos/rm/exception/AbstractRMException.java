package pl.pinkos.rm.exception;

import javax.ejb.ApplicationException;

@ApplicationException(rollback=true)
public class AbstractRMException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8845944982201773503L;
	
	protected AbstractRMException(String message){
		super(message);
	}
	
	protected AbstractRMException(String message, Throwable cause){
		super(message, cause);
	}
	
	

}
