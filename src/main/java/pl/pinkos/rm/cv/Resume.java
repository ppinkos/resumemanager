package pl.pinkos.rm.cv;

import java.util.List;

import pl.pinkos.rm.model.entities.Address;
import pl.pinkos.rm.model.entities.Education;
import pl.pinkos.rm.model.entities.Experience;
import pl.pinkos.rm.model.entities.Person;
import pl.pinkos.rm.model.entities.Skill;

public class Resume {
	
	private Person person;
	
	private Address address;
	
	private List<Education> educations = null;
	
	private List<Experience> experiences = null;
	
	private List<Skill> skills = null;
	
	public Resume(Person person){
		this.person=person;
	}

	public Person getPerson() {
		return person;
	}

	public void setPerson(Person person) {
		this.person = person;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public List<Education> getEducations() {
		return educations;
	}

	public void setEducations(List<Education> educations) {
		this.educations = educations;
	}

	public List<Experience> getExperiences() {
		return experiences;
	}

	public void setExperiences(List<Experience> experiences) {
		this.experiences = experiences;
	}

	public List<Skill> getSkills() {
		return skills;
	}

	public void setSkills(List<Skill> skills) {
		this.skills = skills;
	}
	
	

}
