package pl.pinkos.rm.log;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.Resource;
import javax.ejb.SessionContext;
import javax.interceptor.AroundInvoke;
import javax.interceptor.InvocationContext;

public class LogInterceptor {

	@Resource
	private SessionContext sessionContext;
	
	@AroundInvoke
	public Object logInvokedMethod(InvocationContext invocationContext) throws Exception{
		StringBuilder sb = new StringBuilder();
		sb = sb.append("USER: ")
				.append(sessionContext.getCallerPrincipal().getName())
				.append(" | ").append("Business method: ")
				.append(invocationContext.getTarget().getClass().getName())
				.append(".").append(invocationContext.getMethod().getName());
		
		Object[] parameters = invocationContext.getParameters();
		for (Object param : parameters) {
			if(param != null){
				sb.append(" param: ")
				.append(param.getClass().getName())
				.append(" = ")
				.append(param);
			} else {
				sb.append(" with null value");
			}
		}
		
		Object result;
		try {
			result = invocationContext.proceed();
		
		
		if(result != null){
			sb.append(" returned: ")
			.append(result.getClass().getName())
			.append(" = ")
			.append(result);
		} else {
			sb.append(" returned null");
		}
		
		return result;
		
		} catch (Exception e) {
			sb.append(e);
			throw e;
		}
		finally {
			Logger.getLogger("pl.pinkos").log(Level.INFO, sb.toString());
		}
	}
}
