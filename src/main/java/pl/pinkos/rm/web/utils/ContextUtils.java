package pl.pinkos.rm.web.utils;


import java.util.Locale;
import java.util.ResourceBundle;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;

public class ContextUtils {
	
	public static FacesContext getFacesContext(){
		return FacesContext.getCurrentInstance();
	}
	
	public static void invalidateSession(){
		getFacesContext().getExternalContext().invalidateSession();
	}
	
	public static boolean isUserLogedIn(){
		return getFacesContext().getExternalContext().getRemoteUser()==null ? false : true;
	}
	
	public static String getLoggedUserName(){
		return getFacesContext().getExternalContext().getRemoteUser();
	}
	
	public static ResourceBundle getDefaultBundle(Locale locale) {
        String bundlePath = getFacesContext().getExternalContext().getInitParameter("resourceBundle.path");
        if (null == bundlePath) {
            return null;
        } else {
            return ResourceBundle.getBundle(bundlePath, locale);
        }
    }
	
    public static void createFacesMessage(String componentKey, String boundleKey, String componentIid, FacesMessage.Severity severity, Locale locale) {
    	
        FacesMessage message = new FacesMessage(severity, ContextUtils.getDefaultBundle(locale).getString(boundleKey) + " ", ContextUtils.getDefaultBundle(locale).getString(componentKey));
        getFacesContext().addMessage(componentIid, message); 
        getFacesContext().getExternalContext().getFlash().setKeepMessages(true);
        
    }	


}
