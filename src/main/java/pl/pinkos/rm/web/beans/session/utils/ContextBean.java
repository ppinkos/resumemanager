package pl.pinkos.rm.web.beans.session.utils;

import java.io.Serializable;
import java.util.Locale;

import javax.annotation.PostConstruct;
import javax.enterprise.context.SessionScoped;
import javax.faces.application.FacesMessage;
import javax.inject.Inject;
import javax.inject.Named;

import pl.pinkos.rm.ejb.endpoint.endpoints.UserEndpoint;
import pl.pinkos.rm.model.entities.Person;
import pl.pinkos.rm.model.entities.User;
import pl.pinkos.rm.web.utils.ContextUtils;

@Named
@SessionScoped
public class ContextBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6446201620975812822L;

	@Inject
	private UserEndpoint userEndpoint;

	private Locale locale;

	@PostConstruct
	public void init() {
		locale = ContextUtils.getFacesContext().getExternalContext().getRequestLocale();
	}

	public String invalidateSession() {
		ContextUtils.invalidateSession();
		return "logout";
	}

	public boolean isUserLogedIn() {
		return ContextUtils.isUserLogedIn();
	}

	public void setLocale(String language) {
		this.locale = new Locale(language);
		ContextUtils.getFacesContext().getViewRoot().setLocale(locale);
	}

	public Locale getLocale() {
		return locale;
	}

	public User getLoggedUser() {
		return userEndpoint.getUserByUserName(ContextUtils.getLoggedUserName());
	}

	public Person getLoggedPerson() {
		return getLoggedUser().getPerson();
	}
	
	public void createErrorMessage(String componentKey, String boundleKey){
		ContextUtils.createFacesMessage(componentKey, boundleKey, null, FacesMessage.SEVERITY_ERROR, locale);
	}
	
	public void createWarningMessage(String componentKey, String boundleKey){
		ContextUtils.createFacesMessage(componentKey, boundleKey, null, FacesMessage.SEVERITY_WARN, locale);
	}
	
	public void createInfoMessage(String componentKey, String boundleKey){
		ContextUtils.createFacesMessage(componentKey, boundleKey, null, FacesMessage.SEVERITY_INFO, locale);
	}
	


}
