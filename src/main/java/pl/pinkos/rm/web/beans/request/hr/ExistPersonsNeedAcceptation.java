package pl.pinkos.rm.web.beans.request.hr;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

import pl.pinkos.rm.web.beans.session.hr.ResumeBean;

@Named
@RequestScoped
public class ExistPersonsNeedAcceptation {
	
	@Inject
	private ResumeBean resumeBean;
	
	public boolean existPersonsNeedAcceptation(){
		return resumeBean.existPersonsNeedAcceptation();
	}

}
