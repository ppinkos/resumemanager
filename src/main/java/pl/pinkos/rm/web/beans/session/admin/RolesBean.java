package pl.pinkos.rm.web.beans.session.admin;

import java.io.Serializable;
import java.util.List;

import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;

import pl.pinkos.rm.ejb.endpoint.endpoints.RoleEndpoint;
import pl.pinkos.rm.model.entities.Role;

@SessionScoped
public class RolesBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7980668108997483911L;

	@Inject
	private RoleEndpoint roleEndpoint;
	
	public List<Role> getAllRoles(){
		return roleEndpoint.findAll();
	}
	
	public Role getRoleById(long id){
		return roleEndpoint.findById(id);
	}
}
