package pl.pinkos.rm.web.beans.request.personal.personalDetails;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

import pl.pinkos.rm.exception.RMException;
import pl.pinkos.rm.model.entities.Person;
import pl.pinkos.rm.web.beans.session.personal.personalDetails.PersonBean;
import pl.pinkos.rm.web.beans.session.utils.ContextBean;

import java.io.Serializable;

@Named
@RequestScoped
public class EditPersonalDetailsPageBean implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -2563497741181010334L;

	@Inject
	private PersonBean personBean;
	
	@Inject
	private ContextBean contextBean;

	private Person person;
	
	public Person getPerson(){
		return person;
	}
	
	@PostConstruct
	private void init(){
		person = personBean.getPersonForEdit();
	}
	
	public String savePerson(){
		try {
			personBean.saveEditedPerson();
		} catch (RMException e) {
			contextBean.createErrorMessage("message.module.person", e.getMessage());
			return "personalDetails";
		}
		contextBean.createInfoMessage("message.module.person", "message.info.hasBeenSaved");
		return "personalDetails";
	}
	
	public String cancelEdit(){
		contextBean.createWarningMessage("message.module.person", "message.warning.cancel");
		return "personalDetails";
	}

}
