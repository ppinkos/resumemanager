package pl.pinkos.rm.web.beans.request.personal.experience;

import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

import pl.pinkos.rm.model.entities.Experience;
import pl.pinkos.rm.web.beans.session.personal.experience.ExperienceBean;
import pl.pinkos.rm.web.beans.session.utils.ContextBean;

@Named
@RequestScoped
public class ExperiencePageBean {

	@Inject
	private ExperienceBean experienceBean;
	
	@Inject
	private ContextBean contextBean;
	
	public List<Experience> getExperiences(){
		return experienceBean.getExperienceForPerson();
	}
	
	public String deleteExperience(long id){
		experienceBean.deleteExperience(id);
		contextBean.createWarningMessage("message.module.experience", "message.warning.hasBeenDeleted");
		return "experience";
	}
	
	public String prepareExperienceForEdit(long id){
		experienceBean.prepareExperienceForEdit(id);
		return "editExperience";
	}
	
	
}
