package pl.pinkos.rm.web.beans.request.admin;

import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

import pl.pinkos.rm.exception.RMException;
import pl.pinkos.rm.model.entities.User;
import pl.pinkos.rm.web.beans.session.admin.UsersBean;
import pl.pinkos.rm.web.beans.session.utils.ContextBean;


@Named
@RequestScoped
public class UsersPageBean {
	
	@Inject
	private UsersBean usersBean;
	
	@Inject
	private ContextBean contextBean;
	
	public List<User> getUsers(){
		return usersBean.getUsers();
	}
	
	public String activateUser(long id){
		try {
			usersBean.activateUser(id);
		} catch (RMException e) {
			contextBean.createErrorMessage("message.module.users", e.getMessage());
			return "users";
		}
		return "users";
	}
	
	public String changeUserRole(long id){
		usersBean.prepareUserForEdit(id);
		return "changeUserRole";
	}
	
	public String changeUserPassword(long id){
		usersBean.prepareUserForEdit(id);
		return "changeUserPassword";
	}
	
	public String lockUser(long id){
		try {
			usersBean.lockUser(id);
		} catch (RMException e) {
			contextBean.createErrorMessage("message.module.users", e.getMessage());
			return "users";
		}
		return "users";
	}
	
	public String unlockUser(long id){
		try {
			usersBean.unlockUser(id);
		} catch (RMException e) {
			contextBean.createErrorMessage("message.module.users", e.getMessage());
			return "users";
		}
		return "users";
	}
}
