package pl.pinkos.rm.web.beans.request.hr;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

import pl.pinkos.rm.cv.Resume;
import pl.pinkos.rm.exception.RMException;
import pl.pinkos.rm.model.entities.Education;
import pl.pinkos.rm.model.entities.Experience;
import pl.pinkos.rm.model.entities.Skill;
import pl.pinkos.rm.web.beans.session.hr.ResumeBean;
import pl.pinkos.rm.web.beans.session.pdf.PDFBean;
import pl.pinkos.rm.web.beans.session.utils.ContextBean;

@Named
@RequestScoped
public class ViewResumePageBean {

	@Inject
	private ResumeBean resumeBean;
	
	@Inject
	private ContextBean contextBean;
	
	@Inject
	private PDFBean pdfBean;
	
	private Resume resume;
	
	@PostConstruct
	private void init(){
		resume = resumeBean.getResumeForHrView();
	}

	public Resume getResume() {
		return resume;
	}

	public void setResume(Resume resume) {
		this.resume = resume;
	}
	
	public String acceptPersonalData(){
		try {
			resumeBean.acceptPerson();
		} catch (RMException e) {
			contextBean.createErrorMessage("message.module.resume", e.getMessage());
			return "viewResume";
		}
		contextBean.createInfoMessage("message.module.person", "message.info.hasBeenAccepted");
		return "viewResume";
	}
	
	public String acceptExperience(Experience experience){
		try {
			resumeBean.acceptExperience(experience);
		} catch (RMException e) {
			contextBean.createErrorMessage("message.module.resume", e.getLocalizedMessage());
			return "viewResume";
		}
		contextBean.createInfoMessage("message.module.experience", "message.info.hasBeenAccepted");
		return "viewResume";
	}
	
	public String acceptEducation(Education education){
		try {
			resumeBean.acceptEducation(education);
		} catch (RMException e) {
			contextBean.createErrorMessage("message.module.resume", e.getMessage());
			return "viewResume";
		}
		contextBean.createInfoMessage("message.module.education", "message.info.hasBeenAccepted");
		return "viewResume";
	}
	
	public String acceptSkill(Skill skill){
		try {
			resumeBean.acceptSkill(skill);
		} catch (RMException e) {
			contextBean.createErrorMessage("message.module.resume", e.getMessage());
			return "viewResume";
		}
		contextBean.createInfoMessage("message.module.skill", "message.info.hasBeenAccepted");
		return "viewResume";
	}
	
	public String rejectResume(){
		try {
			resumeBean.rejectResume();
		} catch (RMException e) {
			contextBean.createErrorMessage("message.module.resume", e.getMessage());
			return "viewResume";
		}
		contextBean.createInfoMessage("message.module.resume", "message.info.resumeHasBeenRejected");

		return "viewResume";
	}
	
	public String acceptAll(){
		try {
			resumeBean.acceptAll();
		} catch (RMException e) {
			contextBean.createErrorMessage("message.module.resume", e.getMessage());
			return "viewResume";
		}
		contextBean.createInfoMessage("message.module.resume", "message.info.hasBeenAccepted");

		return "viewResume";
	}
	
	public String createPDF(){
		try {
			pdfBean.createPDF(resume);
		} catch (RMException e) {
			contextBean.createErrorMessage("message.module.resume", e.getMessage());
			return "viewResume";
		}
		return null;
	}
	
}
