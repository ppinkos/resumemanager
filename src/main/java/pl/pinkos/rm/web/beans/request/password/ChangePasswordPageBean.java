package pl.pinkos.rm.web.beans.request.password;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

import pl.pinkos.rm.ejb.endpoint.endpoints.UserEndpoint;
import pl.pinkos.rm.exception.RMException;
import pl.pinkos.rm.model.entities.User;
import pl.pinkos.rm.web.beans.session.utils.ContextBean;
import pl.pinkos.rm.web.utils.PasswordUtils;

@Named
@RequestScoped
public class ChangePasswordPageBean {
	
	@Inject
	private ContextBean contextBean;
	
	@Inject
	private UserEndpoint userEndpoint;

	private User user;
	
	private String currentPassword;
	
	private String newPassword;
	private String repeatNewPassword;
	
	@PostConstruct
	private void init(){
		user = contextBean.getLoggedUser();
	}
	
	public User getUser(){
		return user;
	}

	
	public String getNewPassword() {
		return newPassword;
	}

	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}

	public String getRepeatNewPassword() {
		return repeatNewPassword;
	}

	public void setRepeatNewPassword(String repeatNewPassword) {
		this.repeatNewPassword = repeatNewPassword;
	}

	public String getCurrentPassword() {
		return currentPassword;
	}

	public void setCurrentPassword(String currentPassword) {
		this.currentPassword = currentPassword;
	}
	
	public String changePassword(){
		if(currentPassword == null || newPassword==null || repeatNewPassword==null || currentPassword.isEmpty() || newPassword.isEmpty() || repeatNewPassword.isEmpty()){
			contextBean.createErrorMessage("message.module.password", "message.error.cantBeEmpty");
			return "changePassword";
		}
		else if(user.getPassword().equals(PasswordUtils.MD5hashString(currentPassword)) && newPassword.equals(repeatNewPassword)){
			user.setPassword(PasswordUtils.MD5hashString(newPassword));
			try {
				userEndpoint.changeUserPassword(user);
			} catch (RMException e) {
				contextBean.createErrorMessage("message.module.password", e.getMessage());
				return "myCV";
			}
			contextBean.createInfoMessage("message.module.password", "message.info.passwordHasBeenChanged");
			return "myCV";
		}
		else {
			contextBean.createErrorMessage("message.module.password", "message.error.not-the-same-password");
			return "changePassword";
		}

	}
	
	
	
	

}
