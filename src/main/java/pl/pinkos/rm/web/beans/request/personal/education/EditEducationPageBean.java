package pl.pinkos.rm.web.beans.request.personal.education;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;

import javax.inject.Inject;
import javax.inject.Named;

import pl.pinkos.rm.exception.RMException;
import pl.pinkos.rm.model.entities.Education;
import pl.pinkos.rm.web.beans.session.personal.education.EducationBean;
import pl.pinkos.rm.web.beans.session.utils.ContextBean;


@Named
@RequestScoped
public class EditEducationPageBean {
	
	@Inject
	private EducationBean educationBean;
	
	@Inject
	private ContextBean contextBean;
	
	private Education education;
	
	@PostConstruct
	private void init(){
		education = educationBean.getEducationForEdit();
	}

	public Education getEducation() {
		return education;
	}
	
	public String updateEducation(){
		try {
			educationBean.updateEducation(education);
		} catch (RMException e) {
			contextBean.createErrorMessage("message.module.education", e.getMessage());
			return "education";
		}
		contextBean.createInfoMessage("message.module.education", "message.info.hasBeenSaved");
		return "education";
	}
	
	

}
