package pl.pinkos.rm.web.beans.request.personal.personalDetails;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;


import pl.pinkos.rm.model.entities.Person;
import pl.pinkos.rm.web.beans.session.personal.personalDetails.PersonBean;

@Named
@RequestScoped
public class PersonalDetailsPageBean {
	
	@Inject
	private PersonBean personBean;

	public Person getPerson() {
		return personBean.getPersonForLoggedUser();
	}
	
	public String editPersonalDetails(){
		return "editPersonalDetails";
	}
	
}
