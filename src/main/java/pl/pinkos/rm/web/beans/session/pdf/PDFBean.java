package pl.pinkos.rm.web.beans.session.pdf;

import java.io.OutputStream;
import java.io.Serializable;
import java.net.URL;

import javax.enterprise.context.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.xhtmlrenderer.pdf.ITextRenderer;

import com.lowagie.text.pdf.BaseFont;

import pl.pinkos.rm.cv.Resume;
import pl.pinkos.rm.exception.RMException;
import pl.pinkos.rm.web.utils.ContextUtils;

@SessionScoped
public class PDFBean implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -8299512760454303533L;

	
	private Resume resumeForPdf;

	public Resume getResumeForPdf() {
		return resumeForPdf;
	}

	private void setResumeForPdf(Resume resumeForPdf) {
		this.resumeForPdf = resumeForPdf;
	}
	

	public void createPDF(Resume resume) throws RMException{
		setResumeForPdf(resume);
		createPDF();
	}
	
	private void createPDF() throws RMException{
		FacesContext facesContext = ContextUtils.getFacesContext();
		ExternalContext externalContext = facesContext.getExternalContext();
		try{
			HttpServletRequest request = (HttpServletRequest)externalContext.getRequest();
			HttpServletResponse response = (HttpServletResponse)externalContext.getResponse();
			ITextRenderer renderer = new ITextRenderer();		
			renderer.setDocument(new URL("http://" + request.getServerName() + ":" + request.getServerPort() + "/ResumeManager/pdf/pdfTemplate.xhtml;jsessionid=" + externalContext.getSessionId(false)).toString());
			
					
			renderer.getFontResolver().addFont("/fonts/Arial.TTF", BaseFont.IDENTITY_H, true);
			renderer.getFontResolver().addFont("/fonts/Helvetica.ttf", BaseFont.IDENTITY_H, true);
            renderer.getFontResolver().addFont("/fonts/TIMES.TTF", BaseFont.IDENTITY_H, true);

			
			response.reset();
			response.setContentType("application/pdf");
			response.setCharacterEncoding("UTF-8");
			response.setHeader("Content-Disposition", "inline); filename=\"resume.pdf\"");

			OutputStream os=response.getOutputStream();
			renderer.layout();
			renderer.createPDF(os);
			
		} catch (Exception ex){
			ex.printStackTrace();
			throw RMException.createExceptionWithPDFGeneration(ex);
		}
		setResumeForPdf(null);
		facesContext.responseComplete();
	}
	

}
