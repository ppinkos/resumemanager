package pl.pinkos.rm.web.beans.request.personal.skill;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

import pl.pinkos.rm.exception.RMException;
import pl.pinkos.rm.model.entities.Skill;
import pl.pinkos.rm.web.beans.session.personal.skill.SkillBean;
import pl.pinkos.rm.web.beans.session.utils.ContextBean;

@Named
@RequestScoped
public class AddSkillPageBean {

	@Inject
	private SkillBean skillBean;
	
	@Inject
	private ContextBean contextBean;
	
	private Skill skill = new Skill();
	
	public Skill getSkill(){
		return skill;
	}
	
	public String saveSkill(){
		try {
			skillBean.saveSkill(skill);
		} catch (RMException e) {
			contextBean.createErrorMessage("message.module.skill", e.getMessage());
			return "skill";
		}
		contextBean.createInfoMessage("message.module.skill", "message.info.hasBeenSaved");
		return "skill";
		
	}
	
	public String cancel(){
		contextBean.createWarningMessage("message.module.skill", "message.warning.cancel");
		return "skill";
	}
}
