package pl.pinkos.rm.web.beans.session.personal.experience;

import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;

import pl.pinkos.rm.ejb.endpoint.endpoints.ExperienceEndpoint;
import pl.pinkos.rm.exception.RMException;
import pl.pinkos.rm.model.entities.Experience;
import pl.pinkos.rm.web.beans.session.utils.ContextBean;

import java.io.Serializable;
import java.util.List;

@SessionScoped
public class ExperienceBean implements Serializable {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 5043938086419091677L;

	@Inject
	private ExperienceEndpoint experienceEndpoint;
	
	@Inject
	private ContextBean contextBean;
	
	private Experience experienceForEdit;
	
	public List<Experience> getExperienceForPerson(){
		return experienceEndpoint.getExperienceForPerson(contextBean.getLoggedPerson());
	}
	
	public void saveExperience(Experience experience) throws RMException{
		experience.setPerson(contextBean.getLoggedPerson());
		experienceEndpoint.saveExperience(experience);
	}
	
	public String deleteExperience(long id){
		experienceEndpoint.deleteExperience(id);
		return "experience";
	}
	
	public void prepareExperienceForEdit(long id){
		experienceForEdit = experienceEndpoint.findById(id);
	}
	
	public Experience getExperienceForEdit(){
		return experienceForEdit;
	}
	
	public void updateExperience(Experience experience) throws RMException{
		experienceEndpoint.updateExperience(experience);
	}

}
