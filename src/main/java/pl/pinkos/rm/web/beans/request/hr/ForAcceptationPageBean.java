package pl.pinkos.rm.web.beans.request.hr;


import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

import pl.pinkos.rm.exception.RMException;
import pl.pinkos.rm.model.entities.Person;
import pl.pinkos.rm.web.beans.session.hr.ResumeBean;
import pl.pinkos.rm.web.beans.session.utils.ContextBean;

@Named
@RequestScoped
public class ForAcceptationPageBean {

	@Inject
	private ResumeBean resumeBean;
	
	@Inject
	private ContextBean contextBean;
	
	public List<Person> getPersonsNeedAcceptaion(){
		return resumeBean.getPersonsNeedAcceptaion();
	}
	
	public String acceptAll(long personId){
		try {
			resumeBean.acceptAll(personId);
		} catch (RMException e) {
			contextBean.createErrorMessage("message.module.resume", e.getMessage());
			return "forAcceptation";
		}
		contextBean.createInfoMessage("message.module.resume", "message.info.hasBeenAccepted");
		return "forAcceptation";
	}
	
	public String prepareForHrVewResume(long personId){
		resumeBean.prepareForHrVewResume(personId);
		return "viewResume";
	}
	
	
}
