package pl.pinkos.rm.web.beans.request.hr;


import java.util.List;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

import pl.pinkos.rm.model.entities.Person;
import pl.pinkos.rm.web.beans.session.hr.ResumeBean;
import pl.pinkos.rm.web.beans.session.utils.ContextBean;

@Named
@RequestScoped
public class SearchResumePageBean {

	@Inject
	private ResumeBean resumeBean;

	@Inject
	private ContextBean contextBean;

	private String searchedName;

	private List<Person> searchResults;

	private List<String> allSkills;

	private List<String> searchSkills;

	@PostConstruct
	private void init() {
		searchResults = resumeBean.getSearchResults();
		allSkills = resumeBean.getDistinctSkillsNames();
	}

	public String prepareForHrVewResume(long personId) {
		resumeBean.prepareForHrVewResume(personId);
		return "viewResume";
	}

	public String searchPersonBySurname() {
		if(searchedName == null || searchedName.isEmpty()){
			contextBean.createErrorMessage("message.module.search", "message.error.wrongSearchCriteria");
			return "searchResume";
		}
		boolean foundSomething = resumeBean.searchPersonBySurname(searchedName);
		if (!foundSomething) {
			contextBean.createWarningMessage("message.module.search", "message.warning.noResultsForSearching");
		}
		return "searchResume";
	}

	public String searchPersonBySkills() {
		if(searchSkills == null || searchSkills.isEmpty()){
			contextBean.createErrorMessage("message.module.search", "message.error.wrongSearchCriteria");
			return "searchResume";
		}

		boolean foundSomething = resumeBean.searchPersonBySkills(searchSkills);
		if (!foundSomething) {
			contextBean.createWarningMessage("message.module.search", "message.warning.noResultsForSearching");
		}
		return "searchResume";
	}

	public String getSearchedName() {
		return searchedName;
	}

	public void setSearchedName(String searchedName) {
		this.searchedName = searchedName;
	}

	public List<Person> getSearchResults() {
		return searchResults;
	}

	public List<String> getAllSkills() {
		return allSkills;
	}

	public List<String> getSearchSkills() {
		return searchSkills;
	}

	public void setSearchSkills(List<String> searchSkills) {
		this.searchSkills = searchSkills;
	}

}
