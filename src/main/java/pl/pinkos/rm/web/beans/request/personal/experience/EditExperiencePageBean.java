package pl.pinkos.rm.web.beans.request.personal.experience;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

import pl.pinkos.rm.exception.RMException;
import pl.pinkos.rm.model.entities.Experience;
import pl.pinkos.rm.web.beans.session.personal.experience.ExperienceBean;
import pl.pinkos.rm.web.beans.session.utils.ContextBean;

@Named
@RequestScoped
public class EditExperiencePageBean {
	
	@Inject
	private ExperienceBean experienceBean;
	
	@Inject
	private ContextBean contextBean;
	
	private Experience experience;
	
	@PostConstruct
	private void init(){
		experience = experienceBean.getExperienceForEdit();
	}

	public Experience getExperience() {
		return experience;
	}
	
	public String updateExperience(){
		try {
			experienceBean.updateExperience(experience);
		} catch (RMException e) {
			contextBean.createErrorMessage("message.module.experience", e.getMessage());
			return "experience";
		}
		contextBean.createInfoMessage("message.module.experience", "message.info.hasBeenSaved");
		return "experience";
	}
	
	

}
