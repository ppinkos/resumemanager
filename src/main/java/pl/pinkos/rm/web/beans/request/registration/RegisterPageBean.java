package pl.pinkos.rm.web.beans.request.registration;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

import pl.pinkos.rm.ejb.endpoint.endpoints.UserEndpoint;
import pl.pinkos.rm.model.entities.Address;
import pl.pinkos.rm.model.entities.Person;
import pl.pinkos.rm.model.entities.User;
import pl.pinkos.rm.web.beans.session.utils.ContextBean;
import pl.pinkos.rm.web.utils.PasswordUtils;

@Named
@RequestScoped
public class RegisterPageBean {
	
	@Inject
	private UserEndpoint userEndpoint;
	
	@Inject
	private ContextBean contextBean;
	
	private User user = new User();
	private Person person = new Person();
	private Address address = new Address();
	
	private String repeatPassword;
	
	public String createNewUser(){
		
		if(user.getPassword().equals(repeatPassword)){
			user.setPassword(PasswordUtils.MD5hashString(user.getPassword()));
		}
		else {
			contextBean.createErrorMessage("changepassword-form.password", "message.error.not-the-same-password");
			return "register";
		}
		
		person.setAddress(address);
		user.setPerson(person);
		userEndpoint.createUser(user);
		
		return "login";
	}
	
	public User getUser(){
		return user;
	}
	
	public Person getPerson(){
		return person;
	}

	public void setRepeatPassword(String repeatPassword) {
		this.repeatPassword = repeatPassword;
	}

	public String getRepeatPassword() {
		return repeatPassword;
	}
	
	public Address getAddress(){
		return address;
	}
	
	

}
