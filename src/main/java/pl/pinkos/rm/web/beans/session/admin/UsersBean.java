package pl.pinkos.rm.web.beans.session.admin;

import java.io.Serializable;
import java.util.List;

import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;

import pl.pinkos.rm.ejb.endpoint.endpoints.UserEndpoint;
import pl.pinkos.rm.exception.RMException;
import pl.pinkos.rm.model.entities.Role;
import pl.pinkos.rm.model.entities.User;

@SessionScoped
public class UsersBean implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 3970276252323474234L;
	
	
	@Inject
	private UserEndpoint userEndpoint;
	
	private User userForEdit;
	
	
	public List<User> getUsers(){
		return userEndpoint.getUsers();
	}
	
	public void activateUser(long id) throws RMException{
		User user = userEndpoint.getUserById(id);
		userEndpoint.activateUser(user);
	}
	
	public void changeUserRole(User user, Role newRole) throws RMException{
		userEndpoint.changeUserRole(user, newRole);
		userForEdit = null;
	}
	
	public void changeUserPassword(User user) throws RMException{
		userEndpoint.changeUserPassword(user);
		userForEdit = null;
	}
	
	public void lockUser(long id) throws RMException{
		User user = userEndpoint.getUserById(id);
		userEndpoint.lockUser(user);
	}
	
	public void unlockUser(long id) throws RMException{
		User user = userEndpoint.getUserById(id);
		userEndpoint.unlockUser(user);
	}
	
	public void prepareUserForEdit(long id){
		userForEdit = userEndpoint.getUserById(id);
	}
	
	public User getUserForEdit(){
		return userForEdit;
	}

	public List<User> getNotActivatedUsers() {
		
		return userEndpoint.getNotActivatedUsers();
	}
}
