package pl.pinkos.rm.web.beans.request.personal.skill;

import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

import pl.pinkos.rm.model.entities.Skill;
import pl.pinkos.rm.web.beans.session.personal.skill.SkillBean;
import pl.pinkos.rm.web.beans.session.utils.ContextBean;

@Named
@RequestScoped
public class SkillPageBean {
	
	@Inject
	private SkillBean skillBean;
	
	@Inject
	private ContextBean contextBean;
	
	public List<Skill> getSkills(){
		return skillBean.getSkillForPerson();
	}
	
	public String deleteEducation(long id){
		skillBean.deleteSkill(id);
		contextBean.createWarningMessage("message.module.skill", "message.warning.hasBeenDeleted");
		return "skill";
	}
	
	public String prepareSkillForEdit(long id){
		skillBean.prepareSkillForEdit(id);
		return "editSkill";
	}
	
	public String deleteSkill(long id){
		skillBean.deleteSkill(id);
		return "skill";
	}

}
