package pl.pinkos.rm.web.beans.request.hr;


import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

import pl.pinkos.rm.exception.RMException;
import pl.pinkos.rm.model.entities.User;
import pl.pinkos.rm.web.beans.session.admin.UsersBean;
import pl.pinkos.rm.web.beans.session.utils.ContextBean;

@Named
@RequestScoped
public class ActivateAccountPageBean {

	@Inject
	private UsersBean usersBean;
	
	@Inject
	private ContextBean contextBean;
	
	public List<User> getNotActivatedUsers(){
		return usersBean.getNotActivatedUsers();
	}
	
	public String activateUser(long id){
		try {
			usersBean.activateUser(id);
		} catch (RMException e) {
			contextBean.createErrorMessage("message.module.user", e.getMessage());
			return "activateAccount";
		}
		contextBean.createInfoMessage("message.module.user", "message.info.userHasBeenActivated");
		return "activateAccount";
	}
}
