package pl.pinkos.rm.web.beans.request.personal.education;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

import pl.pinkos.rm.exception.RMException;
import pl.pinkos.rm.model.entities.Education;
import pl.pinkos.rm.web.beans.session.personal.education.EducationBean;
import pl.pinkos.rm.web.beans.session.utils.ContextBean;


@Named
@RequestScoped
public class AddEducationPageBean {

	@Inject
	private EducationBean educationBean;
	
	@Inject
	private ContextBean contextBean;
	
	private Education education  = new Education();
	
	public Education getEducation(){
		return education;
	}
	
	public String saveEducation(){
		try {
			educationBean.saveEducation(education);
		} catch (RMException e) {
			contextBean.createErrorMessage("message.module.education", e.getMessage());
			return "education";
		}
		contextBean.createInfoMessage("message.module.education", "message.info.hasBeenSaved");
		return "education";
	}
	
	public String cancel(){
		contextBean.createWarningMessage("message.module.education", "message.warning.cancel");
		return "education";
	}
}
