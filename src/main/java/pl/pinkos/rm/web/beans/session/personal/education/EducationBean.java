package pl.pinkos.rm.web.beans.session.personal.education;

import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;

import pl.pinkos.rm.ejb.endpoint.endpoints.EducationEndpoint;
import pl.pinkos.rm.exception.RMException;
import pl.pinkos.rm.model.entities.Education;
import pl.pinkos.rm.web.beans.session.utils.ContextBean;

import java.io.Serializable;
import java.util.List;

@SessionScoped
public class EducationBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6057597460379669014L;
	
	@Inject
	private EducationEndpoint educationEndpoint;
	
	@Inject
	private ContextBean contextBean;
	
	private Education educationForEdit;
	
	public List<Education> getEducationForPerson(){
		return educationEndpoint.getEducationForPerson(contextBean.getLoggedPerson());
	}
	
	public void saveEducation(Education education) throws RMException{
		education.setPerson(contextBean.getLoggedPerson());
		educationEndpoint.saveEducation(education);
	}
	
	public void prepareEducationForEdit(long id){
		educationForEdit = educationEndpoint.findById(id);
	}
	
	public Education getEducationForEdit(){
		return educationForEdit;
	}
	
	public void updateEducation(Education education) throws RMException{
		educationEndpoint.updateEducation(education);
	}
	
	public void deleteEducation(long id){
		educationEndpoint.deleteEducation(id);
	}

}
