package pl.pinkos.rm.web.beans.request.personal.education;

import java.util.List;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

import pl.pinkos.rm.model.entities.Education;
import pl.pinkos.rm.web.beans.session.personal.education.EducationBean;
import pl.pinkos.rm.web.beans.session.utils.ContextBean;

@Named
@RequestScoped
public class EducationPageBean {
	
	@Inject
	private EducationBean educationBean;
	
	@Inject
	private ContextBean contextBean;
	
	public List<Education> getEducations(){
		return educationBean.getEducationForPerson();
	}
	
	public String deleteEducation(long id){
		educationBean.deleteEducation(id);
		contextBean.createWarningMessage("message.module.education", "message.warning.hasBeenDeleted");
		return "education";
	}
	
	public String prepareEducationForEdit(long id){
		educationBean.prepareEducationForEdit(id);
		return "editEducation";
	}
	

}
