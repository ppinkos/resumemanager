package pl.pinkos.rm.web.beans.session.hr;

import java.io.Serializable;
import java.util.List;

import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;

import pl.pinkos.rm.cv.Resume;
import pl.pinkos.rm.ejb.endpoint.endpoints.PersonEndpoint;
import pl.pinkos.rm.ejb.endpoint.endpoints.ResumeEndpoint;
import pl.pinkos.rm.ejb.endpoint.endpoints.SkillEndpoint;
import pl.pinkos.rm.exception.RMException;
import pl.pinkos.rm.model.entities.Education;
import pl.pinkos.rm.model.entities.Experience;
import pl.pinkos.rm.model.entities.Person;
import pl.pinkos.rm.model.entities.Skill;


@SessionScoped
public class ResumeBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3564966039276968135L;

	@Inject
	private PersonEndpoint personEndpoint;

	@Inject
	private ResumeEndpoint resumeEndpoint;
	
	@Inject
	private SkillEndpoint skillEndpoint;

	private Resume resumeForHrView;
	
	private List<Person> searchResults;
	

	public List<Person> getPersonsNeedAcceptaion() {
		return personEndpoint.getPersonsNeedAcceptaion();
	}
	
	public void acceptAll() throws RMException {
		acceptAll(resumeForHrView.getPerson().getId());
		prepareForHrVewResume(resumeForHrView.getPerson().getId());
	}

	public void acceptAll(long personId) throws RMException {
		Person person = personEndpoint.findById(personId);
		resumeEndpoint.acceptAll(person);

	}

	public void prepareForHrVewResume(long personId) {
		Person person = personEndpoint.findById(personId);
		resumeForHrView = resumeEndpoint.getResumeForPerson(person);
	}

	public Resume getResumeForHrView() {
		return resumeForHrView;
	}

	public void acceptPerson() throws RMException {
		resumeEndpoint.acceptPerson(resumeForHrView.getPerson());
	}

	public void acceptExperience(Experience experience) throws RMException {
		resumeEndpoint.acceptExperience(experience);
	}

	public void acceptEducation(Education education) throws RMException {
		resumeEndpoint.acceptEducation(education);
	}

	public void acceptSkill(Skill skill) throws RMException {
		resumeEndpoint.acceptSkill(skill);
	}
	
	public void rejectResume() throws RMException{
		resumeEndpoint.rejectResume(resumeForHrView.getPerson());
		prepareForHrVewResume(resumeForHrView.getPerson().getId());
	}
	
	public boolean searchPersonBySurname(String surnamePattern){
		searchResults = resumeEndpoint.searchPersonBySurName(surnamePattern);
		return searchResults.isEmpty() ? false : true;
	}

	public List<Person> getSearchResults() {
		return searchResults;
	}
	
	public List<String> getDistinctSkillsNames() {
		return skillEndpoint.getDistinctSkillsNames();
	}
	
	public boolean searchPersonBySkills(List<String> skills){
		searchResults = resumeEndpoint.searchPersonBySkills(skills);
		return searchResults.isEmpty() ? false : true;
	}
	
	public boolean existPersonsNeedAcceptation(){
		return personEndpoint.existPersonsNeedAcceptation();
	}
	
	

}
