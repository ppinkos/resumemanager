package pl.pinkos.rm.web.beans.request.admin;

import java.util.List;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

import pl.pinkos.rm.exception.RMException;
import pl.pinkos.rm.model.entities.Role;
import pl.pinkos.rm.model.entities.User;
import pl.pinkos.rm.web.beans.session.admin.RolesBean;
import pl.pinkos.rm.web.beans.session.admin.UsersBean;
import pl.pinkos.rm.web.beans.session.utils.ContextBean;

@Named
@RequestScoped
public class ChangeUserRolePageBean {
	
	@Inject
	private UsersBean userBean;
	
	@Inject
	private RolesBean roleBean;
	
	@Inject
	private ContextBean contextBean;
	
	private User user;
	
	private long selectedRoleId;
	
	private List<Role> roles;
	
	@PostConstruct
	private void init(){
		user = userBean.getUserForEdit();
		roles = roleBean.getAllRoles();
	}
	
	public User getUser(){
		return user;
	}
	
	public List<Role> getRoles(){
		return roles;
	}
	
	public long getSelectedRoleId(){
		return selectedRoleId;
	}
	
	public void setSelectedRoleId(long selectedRoleId){
		this.selectedRoleId=selectedRoleId;
	}
	
	public String saveUser(){
		Role newRole = roleBean.getRoleById(selectedRoleId);
		try {
			userBean.changeUserRole(user, newRole);
		} catch (RMException e) {
			contextBean.createErrorMessage("message.module.users", e.getMessage());
			return "users";
		}
		return "users";
	}

}
