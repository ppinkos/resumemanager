package pl.pinkos.rm.web.beans.request.pdf;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

import pl.pinkos.rm.cv.Resume;
import pl.pinkos.rm.web.beans.session.pdf.PDFBean;

@Named
@RequestScoped
public class PdfGeneratorPageBean {
	
	@Inject
	private PDFBean pdfBean;
	
	public Resume getResume(){
		return pdfBean.getResumeForPdf();
	}

}
