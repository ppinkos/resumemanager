package pl.pinkos.rm.web.beans.session.personal.personalDetails;

import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;


import pl.pinkos.rm.ejb.endpoint.endpoints.PersonEndpoint;
import pl.pinkos.rm.exception.RMException;
import pl.pinkos.rm.model.entities.Person;
import pl.pinkos.rm.web.beans.session.utils.ContextBean;

import java.io.Serializable;

@SessionScoped
public class PersonBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8533541625307429276L;

	@Inject
	private PersonEndpoint personEndpoint;

	@Inject
	private ContextBean contextBean;

	private Person personForEdit;

	public Person getPersonForLoggedUser() {
		return personEndpoint.getPersonForUser(contextBean.getLoggedUser());
	}

	public Person getPersonForEdit() {
		this.personForEdit = getPersonForLoggedUser();
		return personForEdit;
	}
	

	public void saveEditedPerson() throws RMException {
		personEndpoint.update(personForEdit);
	}
}
