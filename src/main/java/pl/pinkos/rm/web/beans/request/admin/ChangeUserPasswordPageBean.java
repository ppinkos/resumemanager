package pl.pinkos.rm.web.beans.request.admin;

import javax.annotation.PostConstruct;
import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

import pl.pinkos.rm.exception.RMException;
import pl.pinkos.rm.model.entities.User;
import pl.pinkos.rm.web.beans.session.admin.UsersBean;
import pl.pinkos.rm.web.beans.session.utils.ContextBean;
import pl.pinkos.rm.web.utils.PasswordUtils;

@Named
@RequestScoped
public class ChangeUserPasswordPageBean {

	@Inject
	private UsersBean usersBean;
	
	@Inject
	private ContextBean contextBean;

	private User user;

	private String repeatPassword;

	@PostConstruct
	private void init() {
		user = usersBean.getUserForEdit();
	}

	public User getUser() {
		return user;
	}

	public String getRepeatPassword() {
		return repeatPassword;
	}

	public void setRepeatPassword(String repeatPassword) {
		this.repeatPassword = repeatPassword;
	}

	public String changePassword() {
		if (repeatPassword.equals(user.getPassword())) {
			user.setPassword(PasswordUtils.MD5hashString(user.getPassword()));
			try {
				usersBean.changeUserPassword(user);
			} catch (RMException e) {
				contextBean.createErrorMessage("changepassword-form.password", e.getMessage());
				return "changeUserPassword";
			}
		} else {
			contextBean.createErrorMessage("changepassword-form.password", "message.error.not-the-same-password");
			return "changeUserPassword";
		}
		return "users";
	}

}
