package pl.pinkos.rm.web.beans.request.personal;


import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

import pl.pinkos.rm.cv.Resume;
import pl.pinkos.rm.ejb.endpoint.endpoints.ResumeEndpoint;
import pl.pinkos.rm.exception.RMException;
import pl.pinkos.rm.web.beans.session.pdf.PDFBean;
import pl.pinkos.rm.web.beans.session.utils.ContextBean;

@Named
@RequestScoped
public class MyResumePageBean {
	
	@Inject
	private ContextBean contextBean;
	
	@Inject
	private ResumeEndpoint resumeEndpoint;
	
	@Inject
	private PDFBean pdfBean;
	
	public Resume getResume(){
		return resumeEndpoint.getResumeForPerson(contextBean.getLoggedPerson());
	}
	
	public String createPDF(){
		try {
			pdfBean.createPDF(getResume());
		} catch (RMException e) {
			contextBean.createErrorMessage("message.module.resume", e.getMessage());
			return "myCV";
		}		
		return null;
	}
	
	

}
