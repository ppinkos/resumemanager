package pl.pinkos.rm.web.beans.session.personal.skill;

import javax.enterprise.context.SessionScoped;
import javax.inject.Inject;

import pl.pinkos.rm.ejb.endpoint.endpoints.SkillEndpoint;
import pl.pinkos.rm.exception.RMException;
import pl.pinkos.rm.model.entities.Skill;
import pl.pinkos.rm.web.beans.session.utils.ContextBean;

import java.io.Serializable;
import java.util.List;

@SessionScoped
public class SkillBean implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4426594606732218042L;
	
	@Inject
	private SkillEndpoint skillEndpoint;
	
	@Inject
	private ContextBean contextBean;
	
	private Skill skillForEdit;
	
	public List<Skill> getSkillForPerson(){
		return skillEndpoint.getSkillForPerson(contextBean.getLoggedPerson());
	}
	
	public void deleteSkill(long id){
		skillEndpoint.deleteSkill(id);
	}
	
	public void prepareSkillForEdit(long id){
		skillForEdit = skillEndpoint.findById(id);
	}
	
	public Skill getSkillForEdit(){
		return skillForEdit;
	}
	
	public void saveSkill(Skill skill) throws RMException{
		skill.setPerson(contextBean.getLoggedPerson());
		skillEndpoint.saveSkill(skill);
	}
	
	public void updateSkill(Skill skill) throws RMException{
		skillEndpoint.updateSkill(skill);
	}

}
