package pl.pinkos.rm.web.beans.request.personal.experience;

import javax.enterprise.context.RequestScoped;
import javax.inject.Inject;
import javax.inject.Named;

import pl.pinkos.rm.exception.RMException;
import pl.pinkos.rm.model.entities.Experience;
import pl.pinkos.rm.web.beans.session.personal.experience.ExperienceBean;
import pl.pinkos.rm.web.beans.session.utils.ContextBean;

@Named
@RequestScoped
public class AddExperiencePageBean {

	@Inject
	private ExperienceBean experienceBean;
	
	@Inject
	private ContextBean contextBean;
	
	private Experience experience  = new Experience();
	
	public Experience getExperience(){
		return experience;
	}
	
	public String saveExperience(){
		try {
			experienceBean.saveExperience(experience);
		} catch (RMException e) {
			contextBean.createErrorMessage("message.module.experience", e.getMessage());
			return "experience";
		}
		contextBean.createInfoMessage("message.module.experience", "message.info.hasBeenSaved");
		return "experience";
	}
	
	public String cancel(){
		contextBean.createWarningMessage("message.module.experience", "message.warning.cancel");
		return "experience";
	}
}
