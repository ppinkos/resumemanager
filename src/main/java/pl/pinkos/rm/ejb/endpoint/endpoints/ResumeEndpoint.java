package pl.pinkos.rm.ejb.endpoint.endpoints;

import java.util.Iterator;
import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateful;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.interceptor.Interceptors;

import pl.pinkos.rm.cv.Resume;
import pl.pinkos.rm.ejb.endpoint.AbstractEndpoint;
import pl.pinkos.rm.ejb.mail.Mail;
import pl.pinkos.rm.exception.RMException;
import pl.pinkos.rm.log.LogInterceptor;
import pl.pinkos.rm.model.entities.Education;
import pl.pinkos.rm.model.entities.Experience;
import pl.pinkos.rm.model.entities.Person;
import pl.pinkos.rm.model.entities.Skill;
import pl.pinkos.rm.model.enums.AcceptanceStatus;

@LocalBean
@Stateful
@Interceptors(LogInterceptor.class)
@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
public class ResumeEndpoint extends AbstractEndpoint {

	
	@Inject
	private EducationEndpoint educationEndpoint;
	
	@Inject
	private ExperienceEndpoint experienceEndpoint;
	
	@Inject
	private PersonEndpoint personEndpoint;
	
	@Inject
	private SkillEndpoint skillEndpoint;
	
	@Inject
	private Mail mail;

	public Resume getResumeForPerson(Person person) {
		Resume resume = new Resume(person);
		resume.setAddress(person.getAddress());
		resume.setEducations(educationEndpoint.getEducationForPerson(person));
		resume.setExperiences(experienceEndpoint.getExperienceForPerson(person));
		resume.setSkills(skillEndpoint.getSkillForPerson(person));
		return resume;
	}


	public void acceptSkill(Skill skill) throws RMException {
		skillEndpoint.accept(skill);
		Person person = personEndpoint.findById(skill.getPerson().getId());
		checkDataForAcceptationAndSetPersonAcceptanceStatus(person);
	}


	public void acceptSkillById(long skillId) throws RMException {
		acceptSkill(skillEndpoint.findById(skillId));
	}


	public void acceptEducation(Education education) throws RMException {
		educationEndpoint.accept(education);
		Person person = personEndpoint.findById(education.getPerson().getId());
		checkDataForAcceptationAndSetPersonAcceptanceStatus(person);
	}

	public void acceptEducationById(long educationId) throws RMException {
		acceptEducation(educationEndpoint.findById(educationId));
	}


	public void acceptExperience(Experience experience) throws RMException {
		experienceEndpoint.accept(experience);
		Person person = personEndpoint.findById(experience.getPerson().getId());
		checkDataForAcceptationAndSetPersonAcceptanceStatus(person);
	}


	public void acceptExperienceById(long experienceId) throws RMException {
		acceptExperience(experienceEndpoint.findById(experienceId));
	}


	public void acceptPerson(Person person) throws RMException {
		personEndpoint.accept(person);
		checkDataForAcceptationAndSetPersonAcceptanceStatus(person);
	}

	public void acceptPersonById(long personId) throws RMException {
		Person person = personEndpoint.findById(personId);
		acceptPerson(person);
		checkDataForAcceptationAndSetPersonAcceptanceStatus(person);
	}
	
	private void checkDataForAcceptationAndSetPersonAcceptanceStatus(Person person) throws RMException{
		if (educationEndpoint.isEducationForAcceptation(person) || experienceEndpoint.isExperienceForAcceptation(person) ||	skillEndpoint.isSkillForAcceptation(person) || !person.isAccepted()) {
			personEndpoint.setResumeAcceptationStatus(personEndpoint.findById(person.getId()), AcceptanceStatus.NEED_ACCEPTATION);
		} else {
			personEndpoint.setResumeAcceptationStatus(personEndpoint.findById(person.getId()), AcceptanceStatus.ACCEPTED);
		}
		
	}
	
	
	public void acceptAll(Person person) throws RMException{
		for (Iterator<Education> iterator = educationEndpoint.getEducationForAcceptation(person).iterator(); iterator.hasNext();) {
			Education education = (Education) iterator.next();
			acceptEducation(education);		
		}
		for (Iterator<Experience> iterator = experienceEndpoint.getExperienceForAcceptation(person).iterator(); iterator.hasNext();) {
			Experience experience = (Experience) iterator.next();
			acceptExperience(experience);		
		}
		for (Iterator<Skill> iterator = skillEndpoint.getSkillForAcceptation(person).iterator(); iterator.hasNext();) {
			Skill skill = (Skill) iterator.next();
			acceptSkill(skill);		
		}
		acceptPerson(personEndpoint.findById(person.getId()));
	
	}
	
	public void rejectResume(Person person) throws RMException{
		personEndpoint.setResumeAcceptationStatus(person, AcceptanceStatus.REJECTED);
		mail.send(person.getUser(), "RM: Your profile has been rejected", "Your profile has been rejected with reason: " + person.getRejectionReason());
	}

	public List<Person> searchPersonBySurName(String surNamePattern){
		return personEndpoint.searchPersonBySurName(surNamePattern);
	}
	
	public List<Person> searchPersonBySkills(List<String> skills){
		return personEndpoint.searchPersonBySkills(skills);
	}
	


}
