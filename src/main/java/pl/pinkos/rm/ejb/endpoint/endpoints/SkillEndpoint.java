package pl.pinkos.rm.ejb.endpoint.endpoints;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.SessionSynchronization;
import javax.ejb.Stateful;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.interceptor.Interceptors;

import pl.pinkos.rm.ejb.endpoint.AbstractEndpoint;
import pl.pinkos.rm.ejb.entity.fascades.PersonFascade;
import pl.pinkos.rm.ejb.entity.fascades.SkillFascade;
import pl.pinkos.rm.exception.RMException;
import pl.pinkos.rm.log.LogInterceptor;
import pl.pinkos.rm.model.entities.Person;
import pl.pinkos.rm.model.entities.Skill;
import pl.pinkos.rm.model.enums.AcceptanceStatus;

@LocalBean
@Stateful
@Interceptors(LogInterceptor.class)
@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
public class SkillEndpoint extends AbstractEndpoint implements SessionSynchronization {
	
	@Inject
	private SkillFascade skillFascade;
	
	@Inject
	private PersonFascade personFascade;
	
	public List<Skill> getSkillForPerson(Person person){
		return skillFascade.getSkillForPerson(person);
	}
	
	public void deleteSkill(long id){
		skillFascade.remove(skillFascade.findById(id));
	}
	
	public Skill findById(long id){
		return skillFascade.findById(id);
	}
	
	public void saveSkill(Skill skill) throws RMException{
		skill.setAccepted(false);
		Person person = personFascade.findById(skill.getPerson().getId());
		person.setResumeAcceptanceStatus(AcceptanceStatus.NEED_ACCEPTATION);
		personFascade.update(person);
		skillFascade.create(skill);
	}
	
	public void updateSkill(Skill skill) throws RMException{
		skill.setAccepted(false);
		Person person = personFascade.findById(skill.getPerson().getId());
		person.setResumeAcceptanceStatus(AcceptanceStatus.NEED_ACCEPTATION);
		personFascade.update(person);
		skillFascade.update(skill);
	}
	
	public void accept(Skill skill) throws RMException{
		skill.setAccepted(true);
		skillFascade.update(skill);
	}
	
	public boolean isSkillForAcceptation(Person person){
		return !skillFascade.getSkillForAcceptation(person).isEmpty();
	}
	
	public List<Skill> getSkillForAcceptation(Person person){
		return skillFascade.getSkillForAcceptation(person);
	}
	
	public List<String> getDistinctSkillsNames(){
		return skillFascade.getDistinctSkillsNames();
	}

}
