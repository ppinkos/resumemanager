package pl.pinkos.rm.ejb.endpoint.endpoints;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateful;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.interceptor.Interceptors;

import pl.pinkos.rm.ejb.endpoint.AbstractEndpoint;
import pl.pinkos.rm.ejb.entity.fascades.ExperienceFascade;
import pl.pinkos.rm.ejb.entity.fascades.PersonFascade;
import pl.pinkos.rm.exception.RMException;
import pl.pinkos.rm.log.LogInterceptor;
import pl.pinkos.rm.model.entities.Experience;
import pl.pinkos.rm.model.entities.Person;
import pl.pinkos.rm.model.enums.AcceptanceStatus;

@LocalBean
@Stateful
@Interceptors(LogInterceptor.class)
@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
public class ExperienceEndpoint extends AbstractEndpoint {
	
	@Inject
	private ExperienceFascade experienceFascade;
	
	@Inject
	private PersonFascade personFascade;
	
	public List<Experience> getExperienceForPerson(Person person){
		return experienceFascade.getExperienceForPerson(person);
	}
	
	public void saveExperience(Experience experience) throws RMException{
		experience.setAccepted(false);
		Person person = personFascade.findById(experience.getPerson().getId());
		person.setResumeAcceptanceStatus(AcceptanceStatus.NEED_ACCEPTATION);
		personFascade.update(person);
		experienceFascade.create(experience);
	}
	
	public void deleteExperience(long id){
		experienceFascade.remove(experienceFascade.findById(id));	
	}
	
	public Experience findById(long id){
		return experienceFascade.findById(id);
	}
	
	public void updateExperience(Experience experience) throws RMException{
		experience.setAccepted(false);
		Person person = personFascade.findById(experience.getPerson().getId());
		person.setResumeAcceptanceStatus(AcceptanceStatus.NEED_ACCEPTATION);
		personFascade.update(person);
		experienceFascade.update(experience);
	}
	
	public void accept(Experience experience) throws RMException{
		experience.setAccepted(true);
		experienceFascade.update(experience);
	}
	
	public boolean isExperienceForAcceptation(Person person){
		return !experienceFascade.getExperienceForAcceptation(person).isEmpty();
	}
	
	public List<Experience> getExperienceForAcceptation(Person person){
		return experienceFascade.getExperienceForAcceptation(person);
	}

}
