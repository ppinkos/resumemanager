package pl.pinkos.rm.ejb.endpoint.endpoints;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateful;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.interceptor.Interceptors;

import pl.pinkos.rm.ejb.endpoint.AbstractEndpoint;
import pl.pinkos.rm.ejb.entity.fascades.EducationFascade;
import pl.pinkos.rm.ejb.entity.fascades.PersonFascade;
import pl.pinkos.rm.exception.RMException;
import pl.pinkos.rm.log.LogInterceptor;
import pl.pinkos.rm.model.entities.Education;
import pl.pinkos.rm.model.entities.Person;
import pl.pinkos.rm.model.enums.AcceptanceStatus;

@LocalBean
@Stateful
@Interceptors(LogInterceptor.class)
@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
public class EducationEndpoint extends AbstractEndpoint {
	
	@Inject
	private EducationFascade educationFascade;
	
	@Inject 
	private PersonFascade personFascade;
	
	public List<Education> getEducationForPerson(Person person){
		return educationFascade.getEducationForPerson(person);
	}
	
	public void saveEducation(Education education) throws RMException{
		education.setAccepted(false);
		Person person = personFascade.findById(education.getPerson().getId());
		person.setResumeAcceptanceStatus(AcceptanceStatus.NEED_ACCEPTATION);
		personFascade.update(person);
		educationFascade.create(education);
	}
	
	public Education findById(long id){
		return educationFascade.findById(id);
	}
	
	public void updateEducation(Education education) throws RMException{
		education.setAccepted(false);

		Person person = personFascade.findById(education.getPerson().getId());
		person.setResumeAcceptanceStatus(AcceptanceStatus.NEED_ACCEPTATION);
		personFascade.update(person);
		educationFascade.update(education);
	}
	
	public void deleteEducation(long id){
		educationFascade.remove(educationFascade.findById(id));
	}
	
	public void accept(Education education) throws RMException{
		education.setAccepted(true);
		educationFascade.update(education);
	}
	
	public boolean isEducationForAcceptation(Person person){
		return !educationFascade.getEducationForAcceptation(person).isEmpty();
	}
	
	public List<Education> getEducationForAcceptation(Person person){
		return educationFascade.getEducationForAcceptation(person);
	}

}
