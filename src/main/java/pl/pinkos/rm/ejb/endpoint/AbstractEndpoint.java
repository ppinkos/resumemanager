package pl.pinkos.rm.ejb.endpoint;

import java.util.concurrent.ThreadLocalRandom;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.Resource;
import javax.ejb.SessionContext;


public abstract class AbstractEndpoint {
	protected static final Logger LOG = Logger.getLogger("pl.pinkos");

	@Resource
	protected SessionContext sessionContext;

	private String transactionId;

	public void afterBegin() {
		transactionId = Long.toString(System.currentTimeMillis())
				+ ThreadLocalRandom.current().nextLong(Long.MAX_VALUE);
		LOG.log(Level.INFO, "Transaction ID={0} class {1}, USER: {2}",
				new Object[] { transactionId, this.getClass().getName(), sessionContext.getCallerPrincipal().getName() });
	}

	public void beforeCompletion() {
		LOG.log(Level.INFO, "Transaction ID={0} before commit {1}, USER: {2}",
                new Object[]{transactionId, this.getClass().getName(), sessionContext.getCallerPrincipal().getName()});
	}

	public void afterCompletion(boolean committed) {
		LOG.log(Level.INFO, "Transaction ID={0} class {1} result {3}, USER: {2}", 
                new Object[]{transactionId, this.getClass().getName(), sessionContext.getCallerPrincipal().getName(),
                    committed ? "COMMIT" : "ROLLBACK"});
		
	}

}
