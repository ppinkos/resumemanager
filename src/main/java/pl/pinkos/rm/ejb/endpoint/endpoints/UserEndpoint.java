package pl.pinkos.rm.ejb.endpoint.endpoints;

import java.util.List;


import javax.ejb.LocalBean;
import javax.ejb.Stateful;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.interceptor.Interceptors;

import pl.pinkos.rm.ejb.endpoint.AbstractEndpoint;
import pl.pinkos.rm.ejb.entity.fascades.RoleFascade;
import pl.pinkos.rm.ejb.entity.fascades.UserFascade;
import pl.pinkos.rm.exception.RMException;
import pl.pinkos.rm.log.LogInterceptor;
import pl.pinkos.rm.model.entities.Address;
import pl.pinkos.rm.model.entities.Person;
import pl.pinkos.rm.model.entities.Role;
import pl.pinkos.rm.model.entities.User;

@LocalBean
@Stateful
@Interceptors(LogInterceptor.class)
@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
public class UserEndpoint extends AbstractEndpoint {
	
	@Inject
	private UserFascade userFascade;
	
	@Inject
	private RoleFascade roleFascade;
	
	public void createUser(User user){
		Role role = roleFascade.findByName("USER");
		user.setUserRole(role);
		if(user.getPerson()==null){
			user.setPerson(new Person());
		}
		if(user.getPerson().getAddress()==null){
			user.getPerson().setAddress(new Address());
		}
		userFascade.create(user);
	}

	public User getUserByUserName(String userName){
		return userFascade.getUserByUserName(userName);
	}
	
	public List<User> getUsers(){
		return userFascade.getUsers();
	}
	
	public User getUserById(long id){
		return userFascade.findById(id);
	}
	
	//Admin START
	public void activateUser(User user) throws RMException{
		user.setUserActivated(true);
		userFascade.update(user);
	}
	
	public void changeUserRole(User user, Role newRole) throws RMException{
		user.setUserRole(newRole);
		
		userFascade.update(user);
	}
	
	public void changeUserPassword(User user) throws RMException{
		userFascade.update(user);
	}
	
	public void lockUser(User user) throws RMException{
		user.setUserLock(true);
		userFascade.update(user);
	}
	
	public void unlockUser(User user) throws RMException{
		user.setUserLock(false);
		userFascade.update(user);
	}
	//Admin STOP
	
	//HR start
	public List<User> getNotActivatedUsers(){
		return userFascade.getNotActivatedUsers();
	}
	//HR stop

}
