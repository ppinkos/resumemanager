package pl.pinkos.rm.ejb.endpoint.endpoints;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateful;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.interceptor.Interceptors;

import pl.pinkos.rm.ejb.endpoint.AbstractEndpoint;
import pl.pinkos.rm.ejb.entity.fascades.PersonFascade;
import pl.pinkos.rm.exception.RMException;
import pl.pinkos.rm.log.LogInterceptor;
import pl.pinkos.rm.model.entities.Person;
import pl.pinkos.rm.model.entities.User;
import pl.pinkos.rm.model.enums.AcceptanceStatus;


@LocalBean
@Stateful
@Interceptors(LogInterceptor.class)
@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
public class PersonEndpoint extends AbstractEndpoint {
	
	@Inject
	private PersonFascade personFascade;
	
	public void createPerson(Person person){
		person.setAccepted(false);
		personFascade.create(person);
	}
	
	public Person getPersonForUser(User user){
		return findById(user.getPerson().getId());
	}
	
	public void update(Person person) throws RMException{
		person.setAccepted(false);
		person.setResumeAcceptanceStatus(AcceptanceStatus.NEED_ACCEPTATION);
		personFascade.update(person);
	}
	
	public Person findById(long id){
		return personFascade.findById(id);
	}
	
	public List<Person> getPersonsNeedAcceptaion(){
		return personFascade.getPersonsNeedAcceptaion();
	}
	
	public boolean existPersonsNeedAcceptation(){
		return !personFascade.getPersonsNeedAcceptaion().isEmpty();
	}
	
	public void accept(Person person) throws RMException{
		person.setAccepted(true);
		person.setRejectionReason(null);
		personFascade.update(person);
	}
	
	public void setResumeAcceptationStatus(Person person, AcceptanceStatus acceptanceStatus) throws RMException{
		person.setResumeAcceptanceStatus(acceptanceStatus);
		personFascade.update(person);
	}
	
	public List<Person> searchPersonBySurName(String surNamePattern){
		return personFascade.searchBySurName(surNamePattern);
	}
	
	public List<Person> searchPersonBySkills(List<String> skills){
		return personFascade.searchPersonBySkills(skills);
	}
	
	


}
