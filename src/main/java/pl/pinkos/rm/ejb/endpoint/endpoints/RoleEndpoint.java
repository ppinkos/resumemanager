package pl.pinkos.rm.ejb.endpoint.endpoints;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateful;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import javax.interceptor.Interceptors;

import pl.pinkos.rm.ejb.endpoint.AbstractEndpoint;
import pl.pinkos.rm.ejb.entity.fascades.RoleFascade;
import pl.pinkos.rm.log.LogInterceptor;
import pl.pinkos.rm.model.entities.Role;

@LocalBean
@Stateful
@Interceptors(LogInterceptor.class)
@TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
public class RoleEndpoint extends AbstractEndpoint {
	
	@Inject
	private RoleFascade roleFascade;
	
	public List<Role> findAll(){
		return roleFascade.findAll();
	}
	
	public Role findById(long id){
		return roleFascade.findById(id);
	}
}
