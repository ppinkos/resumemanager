package pl.pinkos.rm.ejb.entity.fascades;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import pl.pinkos.rm.ejb.entity.AbstractEntityFascade;
import pl.pinkos.rm.model.entities.Role;

@LocalBean
@Stateless
@TransactionAttribute(TransactionAttributeType.MANDATORY)
public class RoleFascade extends AbstractEntityFascade<Role> {

	public RoleFascade() {
		super(Role.class);
	}

	@PersistenceContext(name="ResumeManager_PU")
	private EntityManager em;

	@Override
	public EntityManager getEntityManager() {
		return em;
	}
	
	public List<Role> findAll(){
		return em.createNamedQuery("Role.findAll", Role.class).getResultList();

	}
	
	public Role findByName(String roleName){
		return (Role)em.createNamedQuery("Role.findByName", Role.class).setParameter(1, roleName).getSingleResult();
	}

}
