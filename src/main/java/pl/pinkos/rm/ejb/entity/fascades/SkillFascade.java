package pl.pinkos.rm.ejb.entity.fascades;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import pl.pinkos.rm.ejb.entity.AbstractEntityFascade;
import pl.pinkos.rm.model.entities.Person;
import pl.pinkos.rm.model.entities.Skill;

@LocalBean
@Stateless
@TransactionAttribute(TransactionAttributeType.MANDATORY)
public class SkillFascade extends AbstractEntityFascade<Skill> {

	@PersistenceContext(name = "ResumeManager_PU")
	private EntityManager em;

	public SkillFascade() {
		super(Skill.class);
	}

	@Override
	public EntityManager getEntityManager() {
		return em;
	}

	public List<Skill> getSkillForPerson(Person person) {
		return getEntityManager().createNamedQuery("Skill.findAllByPerson", Skill.class).setParameter(1, person).getResultList();
	}
	
	public List<Skill> getSkillForAcceptation(Person person) {
		return getEntityManager().createNamedQuery("Skill.getSkillForAcceptation", Skill.class).setParameter(1, person).getResultList();
	}
	
	public List<String> getDistinctSkillsNames(){
		return getEntityManager().createNamedQuery("Skill.getDistinctSkillsNames", String.class).getResultList();
	}

}
