package pl.pinkos.rm.ejb.entity.fascades;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import pl.pinkos.rm.ejb.entity.AbstractEntityFascade;
import pl.pinkos.rm.model.entities.Person;
import pl.pinkos.rm.model.entities.User;

@LocalBean
@Stateless
@TransactionAttribute(TransactionAttributeType.MANDATORY)
public class PersonFascade extends AbstractEntityFascade<Person> {

	public PersonFascade() {
		super(Person.class);
	}

	@PersistenceContext(name = "ResumeManager_PU")
	private EntityManager em;

	@Override
	public EntityManager getEntityManager() {
		return em;
	}

	public Person getPersonForUser(User user) {
		Object result = em.createNamedQuery("Person.findByUser", Person.class).setParameter(1, user.getPerson().getId())
				.getSingleResult();
		return result == null ? new Person() : (Person) result;
	}

	public List<Person> getPersonsNeedAcceptaion() {
		return getEntityManager().createNamedQuery("Person.findPersonsNeedAcceptaion", Person.class).getResultList();
	}

	public List<Person> searchBySurName(String surNamePattern) {
		String searchedText = "%" + surNamePattern.toUpperCase() + "%";
		return getEntityManager().createNamedQuery("Person.searchPersonBySurname", Person.class).setParameter(1, searchedText).getResultList();
	}
	
	public List<Person> searchPersonBySkills(List<String> skills){
		return getEntityManager().createNamedQuery("Skill.findPersonBySkills", Person.class).setParameter(1, skills).getResultList();
	}

}
