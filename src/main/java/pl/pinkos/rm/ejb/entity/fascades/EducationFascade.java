package pl.pinkos.rm.ejb.entity.fascades;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import pl.pinkos.rm.ejb.entity.AbstractEntityFascade;
import pl.pinkos.rm.model.entities.Education;
import pl.pinkos.rm.model.entities.Person;

@LocalBean
@Stateless
@TransactionAttribute(TransactionAttributeType.MANDATORY)
public class EducationFascade extends AbstractEntityFascade<Education> {

	@PersistenceContext(name = "ResumeManager_PU")
	private EntityManager em;

	@Override
	public EntityManager getEntityManager() {
		return em;
	}

	public EducationFascade() {
		super(Education.class);
	}

	public List<Education> getEducationForPerson(Person person) {
		return getEntityManager().createNamedQuery("Education.findAllByPerson", Education.class).setParameter(1, person)
				.getResultList();
	}

	public List<Education> getEducationForAcceptation(Person person) {
		return getEntityManager().createNamedQuery("Education.getEducationForAcceptation", Education.class).setParameter(1, person)
				.getResultList();
	}

}
