package pl.pinkos.rm.ejb.entity;

import javax.persistence.EntityManager;
import javax.persistence.OptimisticLockException;

import pl.pinkos.rm.exception.RMException;
import pl.pinkos.rm.model.AbstractEntity;

public abstract class AbstractEntityFascade<T> {
	
	private Class<T> entityClass;
	
	public AbstractEntityFascade(Class<T> entityClass) {
        this.entityClass = entityClass;
    }
	
	public abstract EntityManager getEntityManager();
	
	public void create(T entity) {
		getEntityManager().persist(entity);
	}
	
	public T findById(long id){
		return getEntityManager().find(entityClass, id);
	}
	
	public void update(T entity) throws RMException {
		try{
			getEntityManager().merge(entity);
			getEntityManager().flush();
		} catch (OptimisticLockException ex) {
			throw RMException.createExceptionWithOptimisticLockKey((AbstractEntity)entity, ex);
		}

	}
	
	public void remove(T entity){
		getEntityManager().remove(entity);
	}


}
