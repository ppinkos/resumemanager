package pl.pinkos.rm.ejb.entity.fascades;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import pl.pinkos.rm.ejb.entity.AbstractEntityFascade;
import pl.pinkos.rm.model.entities.Experience;
import pl.pinkos.rm.model.entities.Person;

@LocalBean
@Stateless
@TransactionAttribute(TransactionAttributeType.MANDATORY)
public class ExperienceFascade extends AbstractEntityFascade<Experience> {

	public ExperienceFascade() {
		super(Experience.class);
	}

	@PersistenceContext(name="ResumeManager_PU")
	private EntityManager em;
	
	@Override
	public EntityManager getEntityManager() {
		return em;
	}
	
	public List<Experience> getExperienceForPerson(Person person){
		return getEntityManager().createNamedQuery("Experience.findAllByPerson", Experience.class).setParameter(1, person).getResultList();
	}
	
	public List<Experience> getExperienceForAcceptation(Person person) {
		return getEntityManager().createNamedQuery("Experience.getExperienceForAcceptation", Experience.class).setParameter(1, person)
				.getResultList();
	}
	
	
	@Override
	public Experience findById(long id) {
		// TODO Auto-generated method stub
		return super.findById(id);
	}

}
