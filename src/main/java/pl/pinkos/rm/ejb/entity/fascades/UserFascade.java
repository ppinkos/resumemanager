package pl.pinkos.rm.ejb.entity.fascades;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import pl.pinkos.rm.ejb.entity.AbstractEntityFascade;
import pl.pinkos.rm.model.entities.User;

@LocalBean
@Stateless
@TransactionAttribute(TransactionAttributeType.MANDATORY)
public class UserFascade extends AbstractEntityFascade<User> {

	public UserFascade() {
		super(User.class);
	}

	@PersistenceContext(name="ResumeManager_PU")
	private EntityManager em;

	@Override
	public EntityManager getEntityManager() {
		return em;
	}
	
	public User getUserByUserName(String userName){
		User result = (User)em.createNamedQuery("User.findUserByName", User.class).setParameter(1, userName).getSingleResult();
		return result==null? new User() : result;
	}
	
	public List<User> getUsers(){
		List<User> result = em.createNamedQuery("User.findAll", User.class).getResultList();
		return result;
	}
	
	public List<User> getNotActivatedUsers(){
		List<User> result = em.createNamedQuery("User.findNotActivated", User.class).getResultList();
		return result;
	}

}
