package pl.pinkos.rm.ejb.entity.fascades;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import pl.pinkos.rm.ejb.entity.AbstractEntityFascade;
import pl.pinkos.rm.model.entities.Address;

@LocalBean
@Stateless
@TransactionAttribute(TransactionAttributeType.MANDATORY)
public class AddressFascade extends AbstractEntityFascade<Address> {

	@PersistenceContext(name="ResumeManager_PU")
	private EntityManager em;
	
	public AddressFascade() {
		super(Address.class);
	}

	@Override
	public EntityManager getEntityManager() {
		return em;
	}

}
