package pl.pinkos.rm.ejb.mail;

import javax.annotation.Resource;
import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import pl.pinkos.rm.exception.RMException;
import pl.pinkos.rm.model.entities.User;

@LocalBean
@Stateless
public class Mail {
	
	@Resource(name="java:jboss/rmMail")
	private Session session;
	
	public void send(User to, String topic, String textMessage) throws RMException{
		try {
			 
            Message message = new MimeMessage(session);
            message.setRecipients(Message.RecipientType.TO, InternetAddress.parse(to.getEmail()));
            message.setFrom(new InternetAddress("resumemanagerpl@gmail.com"));
            message.setSubject(topic);
            message.setText(textMessage);
            Transport.send(message);
 
        } catch (MessagingException e) {
            throw RMException.createExceptionWithEmailSendError(to, e);
        }
	}
}
